<?php
namespace Login\LoginBundle\Entity;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="comment")
 */

class Comment{
    /**
     *@ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    /**
     *@Assert\NotBlank()
     * @Assert\Email()
     * @ORM Column(type="string" length=50)
     */
    
    protected  $email;
    /**
     *Assert\NotBlank()
     * @ORM\Column(type="string",length=50)
     */
    protected $fullname;
    /**
     *Assert\NotBlank()
     * @ORM\Column(type="string",length=255)
     */
    protected $comment;
    
    public function getEmail(){
        return $this->email;
    }
    
    public function setEmail($email){
        $this->email=$email;
    }
    
    public function getFullname(){
        return $this->email;
    }
    
    public function setFullname($fullname){
        $this->fullname=$fullname;
    }
    
    
    
    public function getComment(){
        return $this->comment;
    }
    
    public function setComment($comment){
        $this->comment=$comment;
    }
    
     public function getId(){
        return $this->id;
    }
       
    
}

